# README #

Утилиты-обертки для простого построения асинхронных операций в GWT-коде.

### Параллельное выполнение ###

```
#!java

public class App implements EntryPoint {
    private static RootPanel root = RootPanel.get();
    private MenuServiceAsync menuService = GWT.create(MenuService.class);

    @Override
    public void onModuleLoad() {
        Parallel p = new Parallel();
        final Future<Menu> menuFuture1 = p.future();
        menuService.getMenu(menuFuture1.callback());

        final Future<Menu> menuFuture2 = p.future();
        menuService.getMenu2(menuFuture2.callback());

        p.handle(new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(throwable.getMessage());
            }

            @Override
            public void onSuccess(Void aVoid) {
                List<Item> items = new ArrayList();
                items.addAll(menuFuture1.get().getItems());
                items.addAll(menuFuture2.get().getItems());
                
                for (Item item : items) {
                    root.add(new TextButton(item.getTitle()));
                }
            }
        });
    }
}
```


### Последовательные вызовы ###

```
#!java
public void onRequested(final ClientEditEvent event) {
        final ClientEditor editor = clientEditorProvider.get();

        editor.setTitle(event.getDialogTitle());

        Serial.with(event.getItem())
            .then(new SerialCallback<Client, Void>() { //загрузка формы
                @Override
                public void handle(Client client, AsyncCallback<Void> asyncCallback) throws Exception {
                    driver.initialize(editor);
                    driver.edit(client);
                    editor.doEdit(asyncCallback);
                }
            })
            .then(new SerialCallback<Void, Client>() { //валидация результата
                @Override
                public void handle(Void aVoid, AsyncCallback<Client> asyncCallback) throws Exception {
                    Client item = driver.flush();
                    if (true)
                        asyncCallback.onSuccess(item);
                    else
                        throw new ValidateException("Неверно заполнено поле.");
                }
            })
            .then(new SerialCallback<Client, Client>() { //сохранение
                @Override
                public void handle(Client client, AsyncCallback<Client> asyncCallback) throws Exception {
                    ClientEditEvent.ResponseHandler responseHandler = event.getResponseHandler();
                    responseHandler.onResponsed(client, asyncCallback);
                }
            })
            .run(new AsyncCallback<Client>() { //по итогу
                @Override
                public void onFailure(Throwable throwable) {
                    Window.alert("Ошибка формы: " + throwable.getMessage());
                }

                @Override
                public void onSuccess(Client client) {
                    editor.close();
                }
            });
    }
```