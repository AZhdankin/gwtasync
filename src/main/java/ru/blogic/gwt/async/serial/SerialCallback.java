package ru.blogic.gwt.async.serial;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Main callback, get the power!.
 */
public interface SerialCallback<C, N> {
    void handle(C data, AsyncCallback<N> callback) throws Exception;
}
