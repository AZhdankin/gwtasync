package ru.blogic.gwt.async.serial;


/**
 * Interface for processor thich producing and consumering one data type.
 */
public interface Processor<T> extends SerialCallback<T, T> {
}
