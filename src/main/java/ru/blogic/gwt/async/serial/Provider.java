package ru.blogic.gwt.async.serial;

/**
 * Imutable wrapper for muttable data;
 */
public class Provider<T> {
    private T data;

    public void set(T data){
        this.data = data;
    }

    public T get(){
        return data;
    }
}
