package ru.blogic.gwt.async.serial;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.blogic.gwt.async.common.Consumer;
import ru.blogic.gwt.async.common.Predicate;
import ru.blogic.gwt.async.common.Producer;

/**
 * Serial engine.
 */
public class Serial<IN, OUT> {
    private final SerialCallback<IN, OUT> handler;
    private final Provider<Consumer<Throwable>> failHandlerProvider;

    private Serial(final Producer<OUT> dataProducer) {
        this.handler = new SerialCallback<IN, OUT>() {

            @Override public void handle(IN data, final AsyncCallback<OUT> callback) throws Exception {
                dataProducer.produce(new AsyncCallback<OUT>() {

                    @Override public void onFailure(Throwable throwable) {
                        failHandlerProvider.get().consume(throwable);
                    }

                    @Override public void onSuccess(OUT out) {
                        callback.onSuccess(out);
                    }
                });
            }
        };
        this.failHandlerProvider = new Provider<Consumer<Throwable>>();
    }

    private Serial(SerialCallback<IN, OUT> handler, Provider<Consumer<Throwable>> failHandlerProvider) {
        this.handler = handler;
        this.failHandlerProvider = failHandlerProvider;
    }

    public static <Void, OUT> Serial<Void, OUT> start(Producer<OUT> dataProducer){
        return new Serial<Void, OUT>(dataProducer);
    }

    public static <T> Serial<Void, T> with(final T data){
        return start(new Producer<T>() {
            @Override
            public void produce(AsyncCallback<T> callback) {
                callback.onSuccess(data);
            }
        });
    }

    public <T> Serial<IN, T> then(SerialCallback<OUT, T> callback){
        CallbackWrap<IN, T> wrap = new CallbackWrap<IN, T>();
        SerialCallback<IN, T> newSerialCallback = wrap.transform(failHandlerProvider, this.handler, callback);
        return new Serial<IN, T>(newSerialCallback, failHandlerProvider);
    };

    public <T> Serial<IN, T> when(final Predicate<OUT> predicate, final SerialCallback<OUT, T> trueCallback, final SerialCallback<OUT, T> falseCallback){
        CallbackWrap<IN, T> wrap = new CallbackWrap<IN, T>();
        SerialCallback<IN, T> newSerialCallback = wrap.transform(failHandlerProvider, this.handler, new SerialCallback<OUT, T>() {
            @Override
            public void handle(OUT data, AsyncCallback<T> callback) throws Exception {
                if(predicate.test(data))
                    trueCallback.handle(data, callback);
                else
                    falseCallback.handle(data, callback);
            }
        });
        return new Serial<IN, T>(newSerialCallback, failHandlerProvider);
    }

    public void run(final AsyncCallback<OUT> callback){
        failHandlerProvider.set(new Consumer<Throwable>() {
            @Override public void consume(Throwable th) {
                callback.onFailure(th);
            }
        });

        try {
            handler.handle(null, callback);
        } catch (Throwable th) {
            callback.onFailure(th);
        }
    }
}