package ru.blogic.gwt.async.serial;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.blogic.gwt.async.common.Consumer;

/**
 * Wrap two callbacks: <A,B> and <B,C> to <A,C>
 */
public class CallbackWrap<IN, OUT> {
    public <C> SerialCallback<IN, OUT> transform(final Provider<Consumer<Throwable>> failHandlerProvider, final SerialCallback<IN, C> callback1, final SerialCallback<C, OUT> callback2){
        return new SerialCallback<IN, OUT>() {
            @Override
            public void handle(IN data, final AsyncCallback<OUT> callback) throws Exception {
                try {
                    callback1.handle(data, new AsyncCallback<C>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                            failHandlerProvider.get().consume(throwable);
                        }

                        @Override
                        public void onSuccess(C c) {
                            try {
                                callback2.handle(c, callback);
                            } catch (Throwable th) {
                                failHandlerProvider.get().consume(th);
                            }
                        }
                    });
                } catch(Throwable th) {
                    failHandlerProvider.get().consume(th);
                }
            }
        };
    };
}
