package ru.blogic.gwt.async.common;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Asynchronous interface for producing of data.
 */
public interface Producer<T> {
    void produce(AsyncCallback<T> callback);
}
