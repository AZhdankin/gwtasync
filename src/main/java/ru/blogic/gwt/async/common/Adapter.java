package ru.blogic.gwt.async.common;

/**
 * Synchronous adapter data from P to T.
 */
public interface Adapter<P, T> {
    T adapt(P data);
}