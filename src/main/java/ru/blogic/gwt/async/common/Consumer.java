package ru.blogic.gwt.async.common;

/**
 * Interface for consumer of data.
 */
public interface Consumer<T> {
    void consume(T data);
}
