package ru.blogic.gwt.async.common;

/**
 * Interface for predicate of conditions.
 */
public interface Predicate<T> {
    boolean test(T data);
}
