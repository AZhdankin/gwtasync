package ru.blogic.gwt.async.parallel;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.blogic.gwt.async.common.Adapter;


/**
 * Interface for future-task.
 */
public interface Future<T> {
    T get();
    AsyncCallback<T> callback();
    <P> AsyncCallback<P> callback(Adapter<P, T> callbackAdapter);
}
