package ru.blogic.gwt.async.parallel;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.blogic.gwt.async.common.Adapter;
import ru.blogic.gwt.async.common.Producer;
import ru.blogic.gwt.async.serial.Serial;

/**
 * Parallel engine.
 */
public class Parallel {
    protected enum State {
        INPROGRESS, FAIL, COMPLETE;
    }

    protected State state = State.INPROGRESS;

    private int counter = 0;
    private AsyncCallback<Void> handler = null;
    private Throwable throwable;

    public Parallel() {
        super();
    }

    public static <T> Serial<Void, T> serial(Producer<T> starter) {
        return Serial.start(starter);
    }

    public void handle(AsyncCallback<Void> handler){
        this.handler = handler;

        if(state == State.COMPLETE)
            handler.onSuccess(null);
        else if(state == State.FAIL)
            handler.onFailure(throwable);
    }

    public Future future(){
        this.counter++;
        return new ParallelFuture();
    }

    public class ParallelFuture<T> implements Future<T> {
        private T result = null;

        private ParallelFuture(){}

        @Override
        public AsyncCallback<T> callback(){
            return new ParallelCallback();
        }

        @Override
        public <P> AsyncCallback<P> callback(final Adapter<P, T> callbackAdapter) {
            return new AsyncCallback<P>() {
                final ParallelCallback parallelCallback = new ParallelCallback();
                @Override public void onFailure(Throwable throwable) {
                    parallelCallback.onFailure(throwable);
                }
                @Override public void onSuccess(P p) {
                    parallelCallback.onSuccess(callbackAdapter.adapt(p));
                }
            };
        }

        @Override
        public T get(){
            return result;
        }

        private class ParallelCallback implements AsyncCallback<T> {
            @Override
            public void onFailure(Throwable th) {
                if(state == State.INPROGRESS) {
                    state = State.FAIL;
                    if(handler != null)
                        handler.onFailure(th);
                    else
                        throwable = th;
                }
            }

            @Override
            public void onSuccess(T t) {
                if(state == State.INPROGRESS){
                    result = t;
                    counter--;
                    if(counter == 0){
                        state = State.COMPLETE;
                        if(handler != null)
                            handler.onSuccess(null);
                    }
                }
            }
        }
    }
}
